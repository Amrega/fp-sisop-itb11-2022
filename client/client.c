#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#define PORT 8080

void sendmessage(int sock, char *msg){
    //printf("%s\n", msg);
    send(sock, msg, strlen(msg), 0);
    memset(msg, 0, 1024);
}


int main(int argc, char *argv[]){
    int me = getuid();
    int myprivs = geteuid();
    int opt, root=0, islogin=0, ada_username=0, ada_password=0;
    char username[21];
    char password[21];
    char database[101];
    char buffer[1024] = {0};
    
    if(me == 0 && myprivs == 0){
        root=1;
        //printf("login as root");
    }else{
        //printf("login as user");
    }

    if(!root){
        int ada=0;
        while((opt = getopt(argc, argv, ":u:p:d:")) != -1) 
        { 
            ada=1;
            //printf("\n%c\n",opt);
            switch(opt) 
            { 
                case 'u':
                    ada_username=1;
                    strcpy(username, optarg);
                    break;
                case 'p':
                    ada_password=1;
                    strcpy(password, optarg);
                    break;
                case 'd':
                    strcpy(database, optarg);
                    printf("database : %s\n", optarg);
                    break;
                case '?': 
                    printf("unknown option: %c\n", optopt);
                    return 0;
                    break; 
            } 
        } 
        if(!ada){
            printf("tidak ada argument\n");
            return 0;
        }
        if(!ada_username || !ada_password){
            printf("tidak ada username / passwrd\n");
            return 0;
        }
    }
    

    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    char input[1024];
    if(!root){
        strcpy(input, "LOGIN ");
        strcat(input, username);
        strcat(input, ":");
        strcat(input, password);
        strcat(input, ";");
        sendmessage(sock , input);
        
    }else if(root){
        strcpy(input, "LOGIN root;");
        send(sock , input , strlen(input) , 0);
    }

    read( sock , buffer, 1024);
    printf("%s\n", buffer);

    if(!strcmp(buffer, "Anda berhasil Login\n\n")){
        islogin=1;
    }else if(!strcmp(buffer, "Anda berhasil Login as root\n\n")){
        root=1;
    }

    while(root || islogin){
        
        memset(input, 0, 1024);
        memset(buffer, 0 ,1024);
        fgets(input, 1024, stdin);
        input[strlen(input)-1] = '\0';
        sendmessage(sock , input);
        read( sock , buffer, 1024);

        if(!strcmp(buffer, "LOGGED OUT\n")){
            printf("user logout\n");
            break;
        }
        
        printf("%s\n", buffer);
    } 

    /*
    if(root){
        
        fgets(input, 1001, stdin);
        send(sock , input , strlen(input) , 0);
    }*/
}
