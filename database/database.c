#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include <errno.h>
#define PORT 8080
int root=0;
int islogin=0;
int database_used=0;
char username[101] = {0};
char database[101] = {0};
int auth=0;

void sendmessage(int new_socket, char *msg){
    //printf("%s\n", msg);
    send(new_socket, msg, strlen(msg), 0);
    memset(msg, 0, 1024);
}

void to_log(char *log){
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    FILE *fp;
    fp = fopen("./databases/root_database/log.txt", "a+");
    fprintf(fp, "%4d-%2d-%2d %2d:%2d:%2d:%s:%s\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, username, log);
    fclose(fp);
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char buffers[1024] = {0};
    char message[1024] = {0};
    FILE *fp;
    DIR *dp;
    struct dirent *entry;


    dp = opendir("./databases");
    if(ENOENT == errno){
        mkdir("./databases", 0700);
    }else if(dp){
        closedir(dp);
    }

    dp = opendir("./databases/root_database");
    if(ENOENT == errno){
        mkdir("./databases/root_database", 0700);
    }else if(dp){
        closedir(dp);
    }

    if(access("./databases/root_database/users.txt", F_OK) != 0){
        fp = fopen("./databases/root_database/users.txt", "a+");
        fprintf(fp, "user:password\n");
        fclose(fp);
    }

    if(access("./databases/root_database/permission.txt", F_OK) != 0){
        fp = fopen("./databases/root_database/permission.txt", "a+");
        fprintf(fp, "database:user\n");
        fclose(fp);
    }
    
    if(access("./databases/root_database/log.txt", F_OK) != 0){
        fp = fopen("./databases/root_database/log.txt", "a+");
        fclose(fp);
    }



    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while(1){
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }else{
            printf("client %d masuk.....\n\n", new_socket);
        }
        while(1){

            memset(buffer, 0 ,1024);
            //printf("read time\n");
            read( new_socket , buffer, 1024);
            strcpy(buffers, buffer);
            printf("%s\n", buffer);

            if(buffer[strlen(buffer)-1] != ';'){
                printf("gaada semicolon (;) :(\n");
                strcpy(message, "Jangan lupa semicolon (;) ngab\n");
                sendmessage(new_socket, message);
                continue;
            }else{
                buffer[strlen(buffer)-1] = '\0';
            }

            if(strstr(buffer, "CREATE USER")){
                if(!root){
                    strcpy(message, "only root can do this\n");
                    sendmessage(new_socket, message);
                    continue;
                }
                char usernames[21];
                char password[21];
                char token[21][21], *cek;
        
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " ");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                    
                }

                printf("%d\n", z);
                
                if(!strcmp(token[0], "CREATE") && !strcmp(token[1], "USER") && !strcmp(token[3], "IDENTIFIED") && !strcmp(token[4], "BY") && z == 6){
                    printf("username : %s\npassword : %s\nuser have been created\n", token[2], token[5]);
                    strcpy(usernames, token[2]);
                    strcpy(password, token[5]);

                    fp = fopen("./databases/root_database/users.txt", "a+");
                    fprintf(fp, "%s:%s\n", usernames, password);
                    fclose(fp);
                    strcpy(message, "Berhasil membuat user\n");
                    sendmessage(new_socket, message);

                    to_log(buffers);
                    //memset(username, 0, 21);
                    //memset(password, 0, 21);
                    
                }else{
                    printf("invalid command\n");
                    strcpy(message, "Invalid Command\n");
                    sendmessage(new_socket, message);
                }
                memset(buffer, 0, 1024);
            }else if(strstr(buffer, "LOGIN")){
                char userpass[51];
                char cek[10];
                
                strcpy(cek, strtok(buffer, " "));
                strcpy(userpass, strtok(NULL, " "));
                strcat(userpass, "\n");

                if(!strcmp(userpass, "root\n")){
                    root=1;
                    
                    bzero(username, 101);
                    strcpy(username, "root");

                    printf("root berhasil Login\n", userpass);
                            
                    strcat(message, "Anda berhasil Login as root\n\n");
                    sendmessage(new_socket, message);
                }else{
                    char temp[1024];
                    
                    fp = fopen("./databases/root_database/users.txt", "r");
                    
                    while(fgets(temp, 1024, fp) != NULL){
                        //printf("%s %s\n",temp, userpass);
                        if(!strcmp(temp, userpass)){
                            islogin=1;
                            
                            bzero(username, 100);
                            strcpy(username, strtok(userpass, ":"));
                            
                            //printf("\n--\n%s\n--\n", username);
                            printf("%s berhasil Login\n", username);
                        
                            strcat(message, "Anda berhasil Login\n\n");
                            sendmessage(new_socket, message);

                            fclose(fp);
                            break;
                        }
                    }
                    if(!islogin){
                        printf("gagal login\n");
                        strcat(message, "Anda gagal Login\n\n");
                        sendmessage(new_socket, message);
                        break;
                    }
                }

            }else if(strstr(buffer, "CREATE DATABASE")){
                char token[21][21], *cek;
        
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " ");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                }

                if(!strcmp(token[0], "CREATE") && !strcmp(token[1], "DATABASE") && z == 3){
                    int berhasil=0;
                    //token[2][strlen(token[2])-1] = '\0';
                    dp = opendir("./databases");
                    if(dp != NULL){
                        while ((entry = readdir(dp))){
                            if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) 
                                continue;

                            if(!strcmp(token[2], entry->d_name)){
                                berhasil=1;
                                char sistem[101];
                                strcpy(message, "database already exist\n");
                                sendmessage(new_socket, message);
                                break;
                            }

                        }
                        
                    }
                    if(berhasil){
                        berhasil=0;
                        continue;
                    }
                    strcpy(token[3], "./databases/");
                    strcat(token[3], token[2]);

                    printf("%s\n", token[3]);
                    printf("%d\n", mkdir(token[3], 0700));
                    
                    strcpy(message, "Berhasil membuat database baru\n");
                    sendmessage(new_socket, message);

                    to_log(buffers);
                }else{
                    printf("invalid command\n");
                    strcpy(message, "Invalid Command\n");
                    sendmessage(new_socket, message);
                }

            }else if(!strncmp(buffer, "GRANT PERMISSION", 16)){
                if(!root){
                    strcpy(message, "only root can do this\n");
                    sendmessage(new_socket, message);
                    continue;
                }
                char token[21][21], *cek;
        
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " ");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                }               
                
                if(!strcmp(token[0], "GRANT") && !strcmp(token[1], "PERMISSION") && !strcmp(token[3], "INTO") && z == 5){
                    fp = fopen("./databases/root_database/permission.txt", "a+");
                    
                    int ada=0;
                    dp = opendir("./databases");
                    if(dp != NULL){
                        while ((entry = readdir(dp))){
                            if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) 
                                continue;

                            if(!strcmp(token[2], entry->d_name)){
                                ada=1;
                                break;
                            }

                        }
                        
                    }
                    if(!ada){
                        strcpy(message, "database tidak ada, anjay\n");
                        sendmessage(new_socket, message);
                        continue;
                    }
                    fprintf(fp, "%s:%s\n", token[2], token[4]);
                    fclose(fp);

                    strcpy(message, "Berhasil memberi permission\n");
                    sendmessage(new_socket, message);

                    to_log(buffers);
                }else{
                    printf("invalid command\n");
                    strcpy(message, "Invalid Command\n");
                    sendmessage(new_socket, message);
                }

            }else if(!strncmp(buffer, "USE", 3)){
                char token[21][21], *cek;
        
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " ");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                }

                
                if(!strcmp(token[0], "USE") && z == 2){
                    char temp[1024];
                    char cek[201];
                    //token[1][strlen(token[1])-1] = "\0";

                    database_used=0;
                    bzero(database, 101);

                    if(root){
                        sprintf(cek, "%s:", token[1]);
                    }else{
                        sprintf(cek, "%s:%s", token[1], username);  
                    }
                    fp = fopen("./databases/root_database/permission.txt", "r");
                        
                    //printf("\n--\n%s\n--\n", cek);
                    while(fgets(temp, 1024, fp) != NULL){
                        //printf("%s %s\n",temp, userpass);
                        if(strstr(temp, cek)){
                            database_used = 1;
                            //printf("\n--\n%s\n--\n", username);
                            printf("database %s used\n", token[1]);

                            
                            strcpy(database, token[1]);

                            strcpy(message, "database used\n");
                            sendmessage(new_socket, message);

                            fclose(fp);

                            to_log(buffers);
                            break;
                        }
                    }
                    if(!database_used){
                        printf("fail use %s database\n", token[1]);
                        strcat(message, "fail use database\n");
                        sendmessage(new_socket, message);
                    }  
                    
                }else if(!strcmp(token[0], "root_database")){
                    printf("root_database can only access by root\n");
                    strcpy(message, "root_database can only access by root\n");
                    sendmessage(new_socket, message);
                }else{
                    printf("invalid command\n");
                    strcpy(message, "Invalid Command\n");
                    sendmessage(new_socket, message);
                }
            }else if(!strncmp(buffer, "DROP DATABASE", 13)){
                
                char token[21][21], *cek;
        
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " ");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                }  

                if(!strcmp(token[0], "DROP") && !strcmp(token[1], "DATABASE") && z == 3){
                    if(!strcmp(token[2], "root_database")){
                        printf("dont drop root_database!!\n");
                        strcpy(message, "dont drop root_database!!\n");
                        sendmessage(new_socket, message);
                        continue;
                    }

                    token[2][strlen(token[2]) - 1] = '\0';
                    int berhasil=0;
                    dp = opendir("./databases");
                    if(dp != NULL){
                        while ((entry = readdir(dp))){
                            if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) 
                                continue;

                            if(!strcmp(token[2], entry->d_name)){
                                berhasil=1;
                                char sistem[101];
                                sprintf(sistem, "rm -r ./databases/%s", token[2]);
                                
                                system(sistem);
                                printf("drop %s database success\n", token[2]);

                                strcpy(message, "drop database success\n");
                                sendmessage(new_socket, message);

                                to_log(buffers);
                            }

                        }
                        
                    }
                    if(!berhasil){
                        printf("fail drop %s database\n", token[2]);
                        strcat(message, "fail drop database\n");
                        sendmessage(new_socket, message);
                    }
                }else{
                    printf("invalid command\n");
                    strcpy(message, "Invalid Command\n");
                    sendmessage(new_socket, message);
                }

            }else if(!strcmp(buffer, "LOGOUT")){
                root=0;
                islogin=0;
                database_used=0;
                printf("%s logged out\n", username);
                bzero(database, 101);
                bzero(username, 101);

                strcat(message, "LOGGED OUT\n");
                sendmessage(new_socket, message);
                break;
            }else if(!strncmp(buffer, "CREATE TABLE", 11)){
                if (!database_used) {
                    strcpy(message, "please use database first!\n");
                    sendmessage(new_socket, message);
                    continue;
                }
                char token[21][21], *cek;
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " (),");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                }  
                if(!strcmp(token[0], "CREATE") && !strcmp(token[1], "TABLE") && z >= 5){
                    //int len_table = strlen(token[2]);
                    int berhasil=0;

                    char dirop[100] = "./databases/";
                    strcat(dirop, database);
                    dp = opendir(dirop);
                    if(dp != NULL){
                        while ((entry = readdir(dp))){
                            if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) 
                                continue;

                            char banding[100];
                            //printf("\n%s coba %s\n", entry->d_name,strtok(entry->d_name, " ."));
                            strcpy(banding, strtok(entry->d_name, "."));
                            if(!strcmp(token[2], banding)){
                                berhasil=1;
                                char sistem[101];
                                strcpy(message, "table already exist\n");
                                sendmessage(new_socket, message);
                                break;
                            }

                        }
                        
                    }else{
                        strcpy(message, "database not exist\n");
                        sendmessage(new_socket, message);
                        continue;
                    }
                    if(berhasil){
                        berhasil=0;
                        continue;
                    }
                    char query[200];
                    char data[200];
                    strcpy(data, token[3]);
                    for(int m = 5; m < z; m+=2){
                        strcat(data, ":");
                        strcat(data, token[m]);
                    }
                    
                    sprintf(query, "echo %s > %s/%s.txt", data, dirop, token[2]);
                    system(query);
                    
                    strcpy(message, "Berhasil membuat table baru\n");
                    sendmessage(new_socket, message);

                    to_log(buffers);
                }else{
                    printf("invalid command\n");
                    strcpy(message, "Invalid Command\n");
                    sendmessage(new_socket, message);
                }
            }else if(!strncmp(buffer, "DROP TABLE", 9)){
                if (!database_used) {
                    strcpy(message, "please use database first!\n");
                    sendmessage(new_socket, message);
                    continue;
                }
                char token[21][21], *cek;
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " (),");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                }  

                int berhasil=0;
                char dirop[100] = "./databases/";
                strcat(dirop, database);
                dp = opendir(dirop);
                if(dp != NULL){
                    while ((entry = readdir(dp))){
                        if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) 
                            continue;

                        char banding[100];
                        strcpy(banding, strtok(entry->d_name, "."));
                        if(!strcmp(token[2], banding)){
                            berhasil=1;
                            
                            char query[200];
                            sprintf(query, "rm %s/%s.txt", dirop, token[2]);
                            system(query);

                            strcpy(message, "success drop table\n");
                            sendmessage(new_socket, message);

                            to_log(buffers);
                            break;
                        }

                    }
                    
                }else{
                    strcpy(message, "database not exist\n");
                    sendmessage(new_socket, message);
                    continue;
                }
                if(!berhasil){
                    berhasil=0;

                    strcpy(message, "failed drop table\n");
                    sendmessage(new_socket, message);
                    continue;
                }
            }else if(!strncmp(buffer, "INSERT INTO", 10)){
                if (!database_used) {
                    strcpy(message, "please use database first!\n");
                    sendmessage(new_socket, message);
                    continue;
                }
                char token[21][21], *cek, token2[21][21];
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " (),'");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                }  

                if(z >= 5){
                    char dirop[101];
                    char column[101];
                    sprintf(dirop, "./databases/%s/%s.txt", database, token[2]);
                    fp = fopen(dirop, "r");
                    
                    
                    
                    if(fp){
                        fgets(column, 100, fp);
                        fclose(fp);

                        fp = fopen(dirop, "a+");
                        int z2=0;
                        strcpy(token2[z2], strtok(column, " :"));
                        while(1){
                            z2++;
                            cek = strtok(NULL, " :");
                            if(cek != NULL)
                                strcpy(token2[z2], cek);
                            else
                                break;
                        } 
                        token2[z2-1][strlen(token2[z2-1])-1] = '\0';
                        int gagal=0;
                        char insert[101] = {0}, insert2[21][51] = {0};
                        for(int m = 3; m < z && !gagal; m+=2){
                            for(int m2 = 0; m2 < z2 && !gagal; m2++){
                                //printf("\n|%s:%s|%d\n", token[m], token2[m2], !strcmp(token[m], token2[m2]));
                                if(!strcmp(token[m], token2[m2])){
                                    strcpy(insert2[m2], token[m+1]);
                                    break;
                                }
                                if(m2 + 1 >= z2){
                                    gagal=1;
                                }
                            }
                        }
                        if(gagal){
                            strcpy(message, "Column is not exist\n");
                            sendmessage(new_socket, message);
                            continue;   
                        }
                        for(int m = 0; m < z2; m++){
                            if(strlen(insert2[m])){
                                strcat(insert, insert2[m]);
                            }else{
                                strcat(insert, "(null)");
                            }
                            if(m + 1 < z2){
                                strcat(insert, ":");
                            }else{
                                strcat(insert, "\n");
                            }
                        }

                        fprintf(fp, "%s", insert);
                        fclose(fp);

                        strcpy(message, "Insert Table success\n");
                        sendmessage(new_socket, message);

                        to_log(buffers);
                        continue;

                    }else{
                        strcpy(message, "Table is not exist\n");
                        sendmessage(new_socket, message);
                        continue;
                    }
                }else{
                    strcpy(message, "Invalid Insert Command\n");
                    sendmessage(new_socket, message);
                }


            }else if(!strncmp(buffer,"SELECT", 5)){
                if (!database_used) {
                    strcpy(message, "please use database first!\n");
                    sendmessage(new_socket, message);
                    continue;
                }

                char token[21][21], *cek;
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " (),");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                } 

                if(!strcmp(token[1], "*") && !strcmp(token[2], "FROM") && z >= 3){
                    char temp[512];
                    char dirop[101];
                    sprintf(dirop, "./databases/%s/%s.txt", database, token[3]);
                    fp = fopen(dirop, "r");
                    if(fp){
                        fread(temp, 512, 1, fp);

                        sprintf(message, "Select Succes\n\n--------\n%s\n--------\n\n", temp);
                        sendmessage(new_socket, message);
                        fclose(fp);

                        to_log(buffers);
                    }
                    else{
                        strcpy(message, "Select fail\n");
                        sendmessage(new_socket, message);
                    }
                    
                }else{
                    strcpy(message, "Invalid select command\n");
                    sendmessage(new_socket, message);
                }


            }else if(!strncmp(buffer,"DELETE FROM", 10)){
                if (!database_used) {
                    strcpy(message, "please use database first!\n");
                    sendmessage(new_socket, message);
                    continue;
                }

                char token[21][21], *cek;
                int z=0;
                strcpy(token[z], strtok(buffer, " "));
                while(1){
                    z++;
                    cek = strtok(NULL, " (),");
                    if(cek != NULL)
                        strcpy(token[z], cek);
                    else
                        break;
                }  

                if(z >= 3){
                    char temp[512] ={0};
                    char dirop[101];
                    sprintf(dirop, "./databases/%s/%s.txt", database, token[2]);
                    fp = fopen(dirop, "r");
                    
                    if(fp){
                        fgets(temp, 512, fp);
                        fclose(fp);
                        fp = fopen(dirop, "w");
                        fwrite(temp , 1 , strlen(temp) , fp);
                        fclose(fp);

                        strcpy(message, "success Delete\n");
                        sendmessage(new_socket, message);

                        to_log(buffers);
                    }else{
                        strcpy(message, "fail Delete\n");
                        sendmessage(new_socket, message); 
                    }
                }else{
                    strcpy(message, "Invalid Delete Command\n");
                    sendmessage(new_socket, message); 
                }

            }else if(!strcmp(buffer, "asosidjoijeqwejwqoeijqwojqwoej")){
                auth=1;
                strcpy(message, "sudah auth\n");
                sendmessage(new_socket, message);
            }else if(!strncmp(buffer,"DUMP", 3)){
                char akeh[1001], akeh_temp[501];
                bzero(akeh, 1001);
                if(!auth){
                    strcpy(message, "cannot do this command!\n");
                    sendmessage(new_socket, message);
                    continue;
                }
                if (!database_used) {
                    strcpy(message, "please use database first!\n");
                    sendmessage(new_socket, message);
                    continue;
                }
                char dirop1[201];
                sprintf(dirop1, "./databases/%s", database);
                dp = opendir(dirop1);
                if(dp){
                    while ((entry = readdir(dp))){
                        if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) 
                            continue;

                        //printf("\n\n----\n\n%s\n\n----\n\n", entry->d_name);
                        char tb[101], tb_files[101], dirop[201];
                        char temp[1024];
                        strcpy(tb_files, entry->d_name);
                        strcpy(tb, strtok(entry->d_name, "."));
                        
                        
                        sprintf(akeh_temp, "DROP TABLE %s\n", tb);
                        strcat(akeh, akeh_temp);
                        bzero(akeh_temp, 501);
                        sprintf(dirop, "%s/%s", dirop1, tb_files);
                        fp = fopen(dirop, "r");

                        if(fp){
                            char token[21][21], token2[21][21], *cek;
                            int z=0;
                            fgets(temp, 1024, fp);
                            sprintf(akeh_temp, "CREATE TABLE %s (", tb);
                            strcat(akeh, akeh_temp);
                            bzero(akeh_temp, 501);
                            strcpy(token[z], strtok(temp, " :"));
                            sprintf(akeh_temp, "%s, ",token[z]);
                            strcat(akeh, akeh_temp);
                            bzero(akeh_temp, 501);
                            
                            
                            while(1){
                                z++;
                                cek = strtok(NULL, " :");
                                if(cek != NULL){
                                    strcpy(token[z], cek);
                                    if(token[z][strlen(token[z])-1] == '\n')
                                        token[z][strlen(token[z])-1] = '\0';
                                    sprintf(akeh_temp, "%s, ", token[z]);
                                    strcat(akeh, akeh_temp);
                                    bzero(akeh_temp, 501);
                                }else{
                                    int pan = strlen(akeh);
                                    akeh[pan-1] = '\0';
                                    akeh[pan-2] = '\0';
                                    sprintf(akeh_temp, ");\n\n");
                                    strcat(akeh, akeh_temp);
                                    bzero(akeh_temp, 501);
                                    break;
                                }
                                    

                                // if(cek != NULL){
                                //     sprintf(akeh_temp, ", ");
                                //     strcat(akeh, akeh_temp);
                                //     bzero(akeh_temp, 501);
                                // }
                                    
                            }  
                            z=0;
                            bzero(temp, 1024);
                            while(fgets(temp, 1024, fp) != NULL){
                                z=0;
                                sprintf(akeh_temp, "INSERT INTO %s (", tb);
                                strcat(akeh, akeh_temp);
                                bzero(akeh_temp, 501);
                                strcpy(token2[z], strtok(temp, " :"));
                                sprintf(akeh_temp, "'%s', %s, ",token[z], token2[z]);
                                strcat(akeh, akeh_temp);
                                bzero(akeh_temp, 501);
                                while(1){
                                    z++;
                                    cek = strtok(NULL, " :");
                                    if(cek != NULL){
                                            
                                        strcpy(token2[z], cek);
                                        if(token2[z][strlen(token2[z])-1] == '\n')
                                            token2[z][strlen(token2[z])-1] = '\0';
                                        sprintf(akeh_temp, "'%s', %s, ",token[z], token2[z]);
                                        strcat(akeh, akeh_temp);
                                        bzero(akeh_temp, 501);
                                    }else{
                                        int pan = strlen(akeh);
                                        akeh[pan-1] = '\0';
                                        akeh[pan-2] = '\0';
                                        sprintf(akeh_temp, ");\n");
                                        bzero(temp, 1024);
                                        strcat(akeh, akeh_temp);
                                        bzero(akeh_temp, 501);
                                        break;
                                    }
                                        

                                    // if(cek != NULL){
                                    //     sprintf(akeh_temp, ", ");
                                    //     strcat(akeh, akeh_temp);
                                    //     bzero(akeh_temp, 501);
                                    // }
                                        

                                }

                            }
                            
                            akeh[strlen(akeh)] = '\n';
                        }

                    }
                    
                }else{
                    strcpy(message, "database not exist!\n");
                    sendmessage(new_socket, message);
                }
                //printf("\n%s\n", akeh);
                //strcpy(message, "anjay");
                sendmessage(new_socket, akeh);
                auth=0;
            }else{
                //printf("read time3\n");
                printf("invalid command\n");
                strcpy(message, "Invalid Command\n");
                sendmessage(new_socket, message);
            }
            //printf("read time4\n");
        }
        
    }
    return 0;
}
